from torpy.http.requests import TorRequests
import sys, time, math

def convert_size(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])

with TorRequests() as tor_requests:
    with tor_requests.get_session() as sess:
        start = time.time()
        print("IP from TOR to use download ", sess.get("http://httpbin.org/ip").json())
        response = sess.get("stream_url", stream=True)
        total_length = response.headers.get('content-length')

        print("Size of the File: ", convert_size(int(total_length)))
        print(response.status_code)
        dl = 0
        with open("response.mp4", "wb") as f:
            if total_length is None: # no content length header
                    f.write(response.content)
            else:
                for chunk in response.iter_content(1024):
                    dl += len(chunk)
                    f.write(chunk)
                    done = int(30 * dl / int(total_length))
                    sys.stdout.write("\r[%s%s] %s Mbps %s" % ('=' * done, ' ' * (30-done), dl//(time.time() - start) / 100000, convert_size(dl)))

        print("\nDownload Completed \nTime Elapsed ", int(time.time() - start), " seconds")
